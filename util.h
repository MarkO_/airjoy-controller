/* 
 * File:   util.h
 * Author: matth
 *
 * Created on February 28, 2021, 9:45 PM
 */

#ifndef UTIL_H
#define	UTIL_H

#define delay_us(x) _delay((unsigned long)((x)*(_XTAL_FREQ/4000000.0)))
#define delay_ms(x) _delay((unsigned long)((x)*(_XTAL_FREQ/4000.0)))

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* UTIL_H */
