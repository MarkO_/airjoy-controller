/* 
 * File:   ser.h
 * Author: matth
 *
 * Created on February 28, 2021, 9:28 PM
 */

#ifndef SER_H
#define	SER_H

void print(unsigned short chr);

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* SER_H */
